import { forwardRef } from "react"
import styles from "./form.module.scss"

const TextField = forwardRef(({
  label,
  error,
  helperText,
  placeholder,
  name,
  type = "text"
}, ref) => {

  return (
    <div className={`form-group ${styles.formGroup}${error ? ' ' + styles.error : ''}`}>
      <label htmlFor={name}>{label}</label>
      <input
        type={type}
        className="form-control"
        placeholder={placeholder}
        ref={ref}
        name={name}
      />
      {helperText && <p className={styles.helperText}>{helperText}</p>}
    </div>
  )
})

export default TextField