import { forwardRef } from "react"
import styles from "./form.module.scss"

const FileField = forwardRef(({
  label,
  error,
  helperText,
  name,
  accept
}, ref) => {

  return (
    <div className={`form-group ${styles.formGroup}${error ? ' ' + styles.error : ''}`}>
      <label htmlFor={name}>{label}</label>
      <input
        type="file"
        className="form-control-file"
        ref={ref}
        name={name}
        accept={accept}
      />
      {helperText && <p className={styles.helperText}>{helperText}</p>}
    </div>
  )
})

export default FileField