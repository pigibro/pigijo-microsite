import styles from "./participantList.module.scss"

const Index = ({ items = [] }) => {
  return (
    <div className={styles.participantList}>
      <table>
        <thead>
          <tr>
            <th width="260">Nama</th>
            <th>Institusi</th>
          </tr>
        </thead>
        <tbody>
          {items?.length > 0 && items.map((row, key) =>
            <tr key={key}>
              <td>{row?.name}</td>
              <td>{row?.institution}</td>
            </tr>
          )}
        </tbody>
      </table>
    </div>
  )
}

export default Index
