import styles from "./newsList.module.scss"
import Link from "next/link"
import moment from "moment"

const makeSlug = (id, title) => {
  let titleArr = title.split(' ')
  let titleStr = titleArr.join('_')
  let slug = `${id}-${titleStr}`
  return slug
}

const NewsList = ({ items = [], onePerRow }) => {  
  return (
    <div className={`${styles.newsList}${onePerRow ? ' ' + styles.onePerRow : ''}`}>
      {items.map((row, key) =>
        <News
          key={key}
          news={row}
        />
      )}
    </div>
  )
}

const News = ({ news: { imagePath, createdAt, title, shortDesc, _id } }) => (
  // <Link href="/news/[slug]" as={`/news/${makeSlug(_id, title)}`}>
  //   <a className={styles.news}>
  //     <div className={styles.imageWrapper}>
  //       <img src={imagePath} alt="" />
  //       <div className={styles.date}>{moment(createdAt).format('DD MMMM YYYY')}</div>
  //     </div>
  //     <div className={styles.contentWrapper}>
  //       <h6 className={styles.title}>{title}</h6>
  //       <p className={styles.desc}>{shortDesc}</p>
  //     </div>
  //   </a>
  // </Link>
  <Link href="#" as={`/news/${makeSlug(_id, title)}`}>
    <a className={styles.news}>
      <div className={styles.imageWrapper}>
        <img src={imagePath} alt="" />
        <div className={styles.date}>{moment(createdAt).format('DD MMMM YYYY')}</div>
      </div>
      <div className={styles.contentWrapper}>
        <h6 className={styles.title}>{title}</h6>
        <p className={styles.desc}>{shortDesc}</p>
      </div>
    </a>
  </Link>
  
)

export default NewsList
