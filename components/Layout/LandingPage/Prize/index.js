import styles from './prize.module.scss'

const Index = () => {

  return (
    <section id="prize" className={styles.prize}>
      <div className="container">
        <h4 className={styles.sectionTitle}>Hadiah</h4>
        <div className={styles.grandPrize}>
          <div className={styles.imageWrapper}>
            <img src="/images/prize/bg.png" alt="" />
          </div>
          <div className={styles.prizeWrapper}>
            <div className={styles.prize}>
              <p>
                Pigijo akan memilih 3 video terbaik per-minggu dan mengumumkannya di akun resmi IG <span className={styles.socialMedia}>@pigijo_</span>
              </p>
            </div>
          </div>
        </div>
        <div className={styles.prizes}>
          <Prize
            title="Juara I"
            total="Rp 500.000"
          />
          <Prize
            title="Juara II"
            total="Rp 300.000"
          />
          <Prize
            title="Juara III"
            total="Rp 200.000"
          />
        </div>
      </div>
    </section>
  )
}

const Prize = ({ title, total }) => (
  <div className={styles.prizeItem}>
    <h6 className={styles.title}>{title}</h6>
    <h2 className={styles.total}>{total}</h2>
  </div>
)

export default Index
