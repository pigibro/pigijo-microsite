import styles from './news.module.scss'
import NewsList from '../../../Templates/NewsList'
import Link from 'next/link'

const Index = ({ news = [] }) => {

  return (
    <section id="news" className={styles.news}>
      <div className="container">
        <h4 className={styles.sectionTitle}>Berita</h4>
        <NewsList items={news} />
        <div className={styles.loadMore}>
          <Link href="/news">
            <a className="btn btn-primary">Lihat Selengkapnya</a>
          </Link>
        </div>
      </div>
    </section>
  )
}

export default Index
