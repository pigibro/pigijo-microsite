import styles from './winner.module.scss'

const Index = ({ winners: items }) => {
  return (
    <section id="winner" className={styles.winner}>
      <div className="container">
        <h4 className={styles.sectionTitle}>Pemenang</h4>
        <div className={styles.winnersWrapper}>
          <div className={styles.winnerBox}>
            <h1 className={styles.rank}>Juara <span className="text-primary">Umum</span></h1>
            <p className={styles.name}>Agus Purnawan</p>
            <p className={styles.group}>Dinas Pendidikan A</p>
            <p className={styles.title}>Judul Video Milik Agus Purnawan</p>
          </div>
          <div className={styles.winnerBox}>
            <h1 className={styles.rank}>Juara <span className="text-primary">Favorit</span></h1>
            <p className={styles.name}>Helsinki Muhammad</p>
            <p className={styles.group}>Dinas Pendidikan B</p>
            <p className={styles.title}>Judul Video Milik Helsinki Muhammad</p>
          </div>
        </div>
        <WinnersTable
          category="Objek Wisata"
          items={items}
        />
        <WinnersTable
          category="Budaya"
          items={items}
        />
        <WinnersTable
          category="Kuliner"
          items={items}
        />
        <WinnersTable
          category="Sosok Inspiratif"
          items={items}
        />
      </div>
    </section>
  )
}

const WinnersTable = ({ category, items = [] }) => {
  return (
    <div className={styles.winnersTableRoot}>
      <div className={styles.title}>Kategori <span className="text-primary">{category}</span></div>
      <div className={styles.winnersTableWrapper}>
        <table>
          <thead>
            <tr>
              <th width="128" className="text-center">Juara</th>
              <th width="260">Nama</th>
              <th width="200">Institusi</th>
              <th>Judul Video</th>
            </tr>
          </thead>
          <tbody>
            {items?.length > 0 && items.map((row, key) =>
              <tr key={key}>
                <td className="text-center">{row?.rank}</td>
                <td>{row?.name}</td>
                <td>{row?.institution}</td>
                <td>{row?.videoTitle}</td>
              </tr>
            )}
          </tbody>
        </table>
      </div>
    </div>
  )
}

export default Index
