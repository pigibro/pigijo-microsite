import styles from './hero.module.scss'

const Index = () => {

  return (
    <section id="hero" className={styles.hero}>
      <div className="container">
        <div className="d-flex flex-column align-items-center">
          {/* <h4 className={styles.subtitle}>Ini Indonesiaku 3</h4>
          <h1 className={styles.title}>Lomba Video Pariwisata Pigijo</h1>
          <h4 className={styles.date}>22 Juli – 24 Agustus 2020</h4> */}
          {/* <div className={`${styles.videoContainer} embed-responsive embed-responsive-16by9`}>
            <img src='images/banner-IGI.png' alt='' className="embed-responsive-item" />
            <iframe className="embed-responsive-item" src="https://www.youtube.com/embed/I940BsxAsz4" frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
          </div> */}
        </div>
      </div>
    </section>
  )
}

export default Index
