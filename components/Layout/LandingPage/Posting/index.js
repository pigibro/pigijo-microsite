import React from 'react'
import styles from './posting.module.scss'

const Index = () => {
  return (
    <section id="posting" className={styles.posting}>
      <div className="container">
        <h4 className={styles.sectionTitle}>Masa Posting</h4>
        <div className="row">
          <div className={`${styles.contentWrapper} col-12 col-md-4`}>
            <h6 className={styles.contentTitle}>Season 1</h6>
            <p>8 – 26 Februari 2021</p>
          </div>
          <div className={`${styles.contentWrapper} col-12 col-md-4`}>
            <h6 className={styles.contentTitle}>Season 2</h6>
            <p>1 – 27 Maret 2021</p>
          </div>
          <div className={`${styles.contentWrapper} col-12 col-md-4`}>
            <h6 className={styles.contentTitle}>Season 3</h6>
            <p>5 – 30 April 2021</p>
          </div>
        </div>

      </div>
    </section>
  )
}

export default Index
