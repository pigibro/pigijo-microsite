import styles from './terms.module.scss';

const Terms = () => {
  return (
    <section id="terms" className={styles.terms}>
      <div className="container">
        <h4>Syarat dan Ketentuan</h4>
        <div className="row">
          <div className="col-12 col-md-6">
            <h6>Syarat</h6>
            <ol className="mb-4 mb-md-0">
              <li>Follow Instagram  <a className={styles.ig} href='https://www.instagram.com/pigijo_/'>@pigijo_</a>, <a className={styles.ig} href='https://www.instagram.com/stptrisakti/'>@stptrisakti</a>, Subscribe Youtube Pigijo dan STP Trisakti</li>
              <li>Upload video di INSTAGRAM FEED siswa dengan hashtag #iniindonesiaku3 #lombapigijo3 #pigijo #stptrisakti #supportlocalexpert</li>
              <li>Tulis caption yang menarik dan informatif, Tag & Mention  <a className={styles.ig} href='https://www.instagram.com/pigijo_/'>@pigijo_</a>, <a className={styles.ig} href='https://www.instagram.com/stptrisakti/'>@stptrisakti</a></li>
            </ol>
          </div>
          <div className="col-12 col-md-6">
            <h6>Ketentuan</h6>
            <ol>
              <li>Format Video Portrait atau Square ukuran Instagram Feed.</li>
              <li>Durasi Video 30 detik – 1 Menit.</li>
              <li>Video yang dilombakan akan menjadi hak milik Pigijo dan STP Trisakti.</li>
              <li>Video merupakan video baru dan belum pernah di-publish di media manapun.</li>
            </ol>
          </div>
        </div>
      </div>
    </section>
  )
}

export default Terms;
