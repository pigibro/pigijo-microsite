import styles from './category.module.scss'

const Index = () => {

  return (
    <section id="category" className={styles.category}>
      <div className="container">
        <h4 className={styles.sectionTitle}>Kategori Lomba</h4>
        <div className={styles.categoryWrapper}>
          <Category
            title="Objek Wisata"
            desc="Menceritakan Objek Wisata Alam yang menjadi andalan di daerah masing-masing, yang mungkin masih belum banyak terdengar oleh masyarakat"
            imageSrc="/images/category/1.jpg"
          />
          <Category
            title="Budaya"
            desc="Menceritakan budaya khas yang unik dan menarik"
            imageSrc="/images/category/2.jpg"
          />
          <Category
            title="Kuliner"
            desc="Menceritakan salah satu kekayaan kuliner yang khas dan nikmat untuk disantap"
            imageSrc="/images/category/3.jpg"
          />
          <Category
            title="Sosok Inspiratif"
            desc="Menceritakan sosok inspiratif (local heroes) di daerah, yang sebenarnya telah berbuat banyak untuk masyarakat"
            imageSrc="/images/category/4.jpg"
          />
        </div>
      </div>
    </section>
  )
}

const Category = ({ title, desc, imageSrc }) => (
  <div className={styles.categoryItem}>
    <div className={styles.imageWrapper}>
      <img src={imageSrc} alt="" />
    </div>
    <div className={styles.textWrapper}>
      <h6 className={styles.title}>{title}</h6>
      <p className={styles.desc}>{desc}</p>
    </div>
  </div>
)

export default Index
