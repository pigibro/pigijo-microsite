import styles from './participants.module.scss'
import ParticipantList from '../../../Templates/ParticipantList'
import Link from 'next/link'

const Index = ({ participants = [] }) => {

  return (
    <section id="participants" className={styles.participants}>
      <div className="container">
        <h4 className={styles.sectionTitle}>Peserta</h4>
        <p className={styles.sectionSubtitle}>Daftar peserta yang baru saja mendaftar Lomba Video Pariwisata Pigijo</p>
        <ParticipantList items={participants} />
        <div className={styles.loadMore}>
          <Link href="/participant">
            <a className="btn btn-primary">Lihat Selengkapnya</a>
          </Link>
        </div>
      </div>
    </section>
  )
}

export default Index
