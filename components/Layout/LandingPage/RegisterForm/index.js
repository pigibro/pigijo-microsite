import styles from './registerForm.module.scss';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers';
import * as yup from "yup";
import swal from 'sweetalert'
import TextField from '../../../Forms/TextField';
import FileField from '../../../Forms/FileField';
import { authApi } from '../../../../utils/services/api';
import { useState } from 'react';
import SuccessCard from './SuccessCard';

const phoneNumberRegex = /^\+?[0-9]*$/;
const schema = yup.object().shape({
  name: yup.string().required('Nama tidak boleh kosong'),
  phone: yup.string().required('No Telepon tidak boleh kosong').matches(phoneNumberRegex, 'Format tidak sesuai. Contoh: +628xxxxxxxxxx'),
  member_id: yup.string().required('No Anggota IGI tidak boleh kosong'),
  card_member_path: yup.mixed().test(
    'file',
    'Foto tidak boleh kosong',
    value => value && value.length > 0
  ),
  institution: yup.string().required('Institusi tidak boleh kosong'),
  url_instagram: yup.string().required('Instagram tidak boleh kosong'),
  email: yup.string().email('Email tidak valid').required('Email tidak boleh kosong'),
});

const RegisterForm = () => {
  const [open, setOpen] = useState(false);
  const { register, handleSubmit, errors, reset } = useForm({
    mode: "onBlur",
    resolver: yupResolver(schema),
  });

  const onSubmit = (values) => {

    getBase64(values.card_member_path[0], (cardMemberbase64) => {
      
      const data = {
        ...values,
        card_member_path: cardMemberbase64
      }
      authApi.register(data)
        .then(res => {
          setOpen(true)
          reset()
        })
        .catch(err => {
          console.log();
          let errKey = Object.keys(err)
          let message = []
          if(errKey.length > 0) {
            for(let key in err) {
              message.push(err[key][0])
            }
          }
          swal({
            text: errKey.length > 0 ? `${message.map((mes) => `${mes}\n`)}` : `Periksa kembali semua kolom registrasi atau jaringan internet anda.`,
          })
        })
    });
    
  };

  const getBase64 = (file, cb) => {
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
        cb(reader.result)
    };
    reader.onerror = function (error) {
        console.log('Error: ', error);
    };
  }

  return (
    <section id="registerForm" className={styles.sectionForm}>
      <div className="container">
        <div className="d-flex flex-column align-items-center">
          <h4>Formulir Pendaftaran</h4>
          <form onSubmit={handleSubmit(onSubmit)}>
            <TextField
              label="Nama"
              placeholder="Isi nama lengkap Anda"
              ref={register}
              name="name"
              error={Boolean(errors.name)}
              helperText={errors.name?.message}
            />
            <TextField
              label="No Telepon"
              placeholder="Isi nomor telepon yang dapat dihubungi"
              ref={register}
              name="phone"
              error={Boolean(errors.phone)}
              helperText={errors.phone?.message}
            />
            <TextField
              label="No Anggota IGI"
              placeholder="Isi nomor anggota yang terdaftar di IGI"
              ref={register}
              name="member_id"
              error={Boolean(errors.member_id)}
              helperText={errors.member_id?.message}
            />
            <FileField
              label="Upload Foto Kartu Anggota"
              ref={register}
              name="card_member_path"
              accept='image/png,image/jpeg,image/x-png,image/gif,image/jpg'
              error={Boolean(errors.card_member_path)}
              helperText={errors.card_member_path?.message}
            />
            <TextField
              label="Institusi"
              placeholder="Sebutkan institusi Anda"
              ref={register}
              name="institution"
              error={Boolean(errors.institution)}
              helperText={errors.institution?.message}
            />
            <TextField
              label="URL Instagram"
              placeholder="Isi URL akun instagram yang aktif"
              ref={register}
              name="url_instagram"
              error={Boolean(errors.url_instagram)}
              helperText={errors.url_instagram?.message}
            />
            <TextField
              type="email"
              label="Email"
              placeholder="Isi email yang valid"
              ref={register}
              name="email"
              error={Boolean(errors.email)}
              helperText={errors.email?.message}
            />
            <div className={styles.btnSubmitWrapper}>
              <button type="submit" className="btn btn-primary">Daftar Sekarang</button>
            </div>
          </form>
        </div>
      </div>
      <SuccessCard
        open={open}
        onClose={() => { console.log('test'); setOpen(false) }}
        maxWidth={560}
      />
    </section>
  )
}

export default RegisterForm;
