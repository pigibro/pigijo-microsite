import Dialog from '@material-ui/core/Dialog';
import styles from './registerForm.module.scss';
import { Tick, Ig } from '../../../Icons';

const SuccessCard = ({ open, onClose }) => {
  return (
    <Dialog classes={{ paper: styles.customDialog }} open={open} onClose={onClose}>
      <div className={styles.successCard}>
        <h4>Pendaftaran <span>Berhasil</span></h4>
        <Tick className={styles.successIcon} />
        <p className={styles.infoSuccess}>
          Selamat pendaftaran Anda berhasil.<br />
              Tim IGI akan melakukan proses verifikasi dalam waktu 1x24 jam.<br />
              Anda akan dihubungi via email/telepon pastikan aktif yah!<br />
              Terima kasih sudah bergabung di <span className="font-weight-bold">Lomba Video Pariwisata Pigijo.</span>
        </p>
        <div className={styles.infoFollow}>
          <p className={styles.infoSocmed}>Sambil menunggu, yuk lihat dan follow sosial media kami</p>
          <div className={styles.instagramCardWrapper}>
            <div className={styles.instagramCard}>
              <Ig />
              <p className={styles.instagramUsername}>@pigijo_</p>
            </div>
            <div className={styles.instagramCard}>
              <Ig />
              <p className={styles.instagramUsername}>@pp.lgi</p>
            </div>
            <div className={styles.instagramCard}>
              <Ig />
              <p className={styles.instagramUsername}>@sagusavi</p>
            </div>
          </div>
        </div>
        <button className="btn btn-primary" onClick={onClose}>Tutup</button>
      </div>
    </Dialog>
  )
}

export default SuccessCard;
