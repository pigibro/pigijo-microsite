import styles from './assesment.module.scss'

const Index = () => {

  return (
    <section id="assesment" className={styles.assesment}>
      <div className="container">
        <h4 className={styles.sectionTitle}>Kriteria Penilaian</h4>
        <div className={styles.assesmentWrapper}>
          <Assesment
            title="Unik"
            desc="Bisa dari cara pengambilan, ataupun isi konten yang disajikan."
            imageSrc="/images/assesment-icon/1.jpg"
          />
          <Assesment
            title="Otentik"
            desc="Memiliki ciri khas yang kuat."
            imageSrc="/images/assesment-icon/2.jpg"
          />
          <Assesment
            title="Memiliki pesan yang jelas"
            desc="Viewers mudah memahami apa yang menjadi tujuan konten tersebut."
            imageSrc="/images/assesment-icon/3.jpg"
          />
          <Assesment
            title="Menarik"
            desc="mampu membuat viewers menyukai dan memberi komentar (engage)."
            imageSrc="/images/assesment-icon/4.jpg"
          />
        </div>
      </div>
    </section>
  )
}

const Assesment = ({ title, desc, imageSrc }) => (
  <div className={styles.assesmentItem}>
    <div className={styles.textWrapper}>
      <h6 className={styles.title}>{title}</h6>
      <p className={styles.desc}>{desc}</p>
    </div>
    <div className={styles.imageWrapper}>
      <img src={imageSrc} alt="" />
    </div>
  </div>
)

export default Index
