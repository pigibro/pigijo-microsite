import styles from './videos.module.scss'
import { VideoType } from '../../../../utils/enums/videoType'
import moment from 'moment'
import { useState } from 'react';

const Index = ({ videos = [] }) => {
  const [selectedVideo, setSelectedVideo] = useState(videos.length > 0 ? videos[0] : null);

  return (
    <section id="videos" className={styles.videos}>
      <div className="container">
        <h4 className={styles.sectionTitle}>Video</h4>
        <div className={`row row-cols-1 row-cols-lg-2 ${styles.customRowMargin}`}>
          <div className={`col ${styles.customColPadding}`}>
            <div className={styles.videoRoot}>
              <div className={`${styles.videoContainer} embed-responsive embed-responsive-16by9`}>
                <iframe className="embed-responsive-item" src={`https://www.youtube.com/embed/${selectedVideo.videoId}`} frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
              </div>
              <div className={`badge ${selectedVideo.type === VideoType.TUTORIAL ? 'badge-orange' : 'badge-dark'} ${styles.badge}`}>{VideoType.getStr(selectedVideo.type)}</div>
              <h6 className={styles.title}>{selectedVideo.title}</h6>
              <p className={styles.desc}>{selectedVideo.desc}</p>
            </div>
          </div>
          <div className={`col ${styles.customColPadding}`}>
            <div className={styles.videoListRoot}>
              <div className={styles.videoListContainer}>
                {videos.length > 0 && videos.map((row, key) =>
                  <Video
                    key={key}
                    thumbnail={row.thumbnail}
                    title={row.title}
                    type={row.type}
                    date={row.date}
                    onClick={() => setSelectedVideo(row)}
                    active={selectedVideo.videoId === row.videoId}
                  />
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

const Video = ({ thumbnail, type, title, date, onClick, active }) => (
  <button className={`${styles.video}${active ? ' ' + styles.active : ''}`} onClick={onClick}>
    <div className={styles.thumbnail}>
      <div className={styles.thumbnailWrapper}>
        <img src={thumbnail} alt="" />
      </div>
      <div className={`badge ${type === VideoType.TUTORIAL ? 'badge-orange' : 'badge-dark'} ${styles.badge}`}>{VideoType.getStr(type)}</div>
    </div>
    <div className={styles.info}>
      <h6 className={styles.title}>{title}</h6>
      <p className={styles.date}>{moment(date).format('DD MMMM YYYY')}</p>
    </div>
  </button>
)

export default Index
