import ParticipantList from '../../Templates/ParticipantList'
import styles from "./participantPage.module.scss"

const ParticipantListSection = ({ participants = [] }) => {
  return (
    <section className={styles.participantPage}>
      <div className="container">
        <h1 className={styles.sectionTitle}>Peserta.</h1>
        <ParticipantList items={participants} />
      </div>
    </section>
  )
}

export default ParticipantListSection
