import styles from './layout.module.scss'
import { useState, useCallback } from 'react';
import Fade from '@material-ui/core/Fade';
import { useRouter } from 'next/router';

const Header = () => {
  const [openSidebar, setOpenSidebar] = useState(false);
  const router = useRouter();

  const scrollTo = useCallback(async (e) => {
    e.preventDefault();
    setOpenSidebar(false);
    const href = e.target.getAttribute("href");

    if (router.pathname !== '/') await router.push('/');

    const position = document.querySelector(href).offsetTop;
    const headerHeight = document.querySelector("header")?.clientHeight ?? 0;

    window.scrollTo({
      top: position - headerHeight,
      behavior: 'smooth'
    });
  }, [router.pathname]);

  return (
    <header className={styles.header}>
      <div className="container">
        <div className={styles.headerContent}>
          <HamburgerMenu active={openSidebar} onClick={() => setOpenSidebar(!openSidebar)} />
          <WebLogo />
        </div>
      </div>
      <NavList open={openSidebar} handleClickLink={scrollTo} />
    </header>
  )
}

const HamburgerMenu = ({ active, onClick }) => (
  <button onClick={onClick} className={styles.hamburgerWrapper}>
    <div className={`${styles.hamburgerMenu}${active ? ' ' + styles.active : ''}`}>
      <div></div>
      <div></div>
      <div></div>
    </div>
  </button>
)

const WebLogo = () => (
  <div className={styles.teamsLogo}>
    <a href='https://www.pigijo.com/'>
      <img src="/images/pigijo.png" alt="" />
    </a>
    <a href='https://stptrisakti.ac.id/'>
      <img className={styles.trisakti} src="/images/stptrisakti.png" alt="" />
    </a>
    {/* <a href='https://www.sagusavi.com/?m=1'>
      <img src="/images/sagusavi.png" alt="" />
    </a>
    <a href='https://www.instagram.com/berisikproject/?hl=id'>
      <img src="/images/berisik.png" alt="" />
    </a>
    <img src="/images/indonesia-maju.png" alt="" /> */}
  </div>
)

const NavList = ({ open, handleClickLink = () => { } }) => (
  <Fade in={open} unmountOnExit>
    <div className={styles.navRoot}>
      <div className="container">
        <div className={styles.navWrapper}>
          <ul className={styles.navList}>
            <li className={styles.navItem}>
              <a onClick={handleClickLink} href="#hero" className={styles.navLink}>Beranda</a>
            </li>
            {/* <li className={styles.navItem}>
              <a onClick={handleClickLink} href="#registerForm" className={styles.navLink}>Formulir Pendaftaran</a>
            </li> */}
            <li className={styles.navItem}>
              <a onClick={handleClickLink} href="#terms" className={styles.navLink}>Syarat dan Ketentuan</a>
            </li>
            <li className={styles.navItem}>
              <a onClick={handleClickLink} href="#prize" className={styles.navLink}>Hadiah</a>
            </li>
            <li className={styles.navItem}>
              <a onClick={handleClickLink} href="#category" className={styles.navLink}>Kategori Lomba</a>
            </li>
            <li className={styles.navItem}>
              <a onClick={handleClickLink} href="#assesment" className={styles.navLink}>Kriteria Penilaian</a>
            </li>
          </ul>
          <ul className={styles.navList}>
            {/* <li className={styles.navItem}>
              <a className={styles.navLink}>Tim Juri</a>
            </li> */}
            {/* <li className={styles.navItem}>
              <a onClick={handleClickLink} href="#participants" className={styles.navLink}>Peserta</a>
            </li> */}
            <li className={styles.navItem}>
              <a onClick={handleClickLink} href="#news" className={styles.navLink}>Berita</a>
            </li>
            <li className={styles.navItem}>
              <a onClick={handleClickLink} href="#videos" className={styles.navLink}>Video</a>
            </li>
            {/* <li className={styles.navItem}>
              <a onClick={handleClickLink} href="#winner" className={styles.navLink}>Pemenang</a>
            </li> */}
          </ul>
        </div>
      </div>
    </div>
  </Fade>
)

export default Header
