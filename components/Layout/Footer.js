import styles from './layout.module.scss'

const Footer = () => {
  const backToTop = () => {
    window.scrollTo({
      top: 0,
      left: 0,
      behavior: 'smooth'
    })
  }

  return (
    <footer className={styles.footer}>
      <div className="container">
        <div className={styles.content}>
          <span className={styles.copyright}>&copy; Pigijo x STP Trisakti 2021. All rights reserved</span>
          <button className={styles.backToTop} onClick={backToTop}>Back to top</button>
        </div>
      </div>
    </footer>
  )
}

export default Footer
