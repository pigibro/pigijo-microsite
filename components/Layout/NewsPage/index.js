import NewsList from '../../Templates/NewsList'
import styles from "./newsPage.module.scss"

const NewsListSection = ({ news }) => {  
  return (
    <section className={styles.newsPage}>
      <div className="container">
        <h1 className={styles.sectionTitle}>Berita.</h1>
        <NewsList items={news} />
      </div>
    </section>
  )
}

export default NewsListSection
