import styles from "./newsPage.module.scss"
import NewsList from '../../Templates/NewsList'
import moment from "moment"

const NewsDetailSection = ({ newsDetail, otherNews }) => {  
  return (
    <section className={styles.newsDetail}>
      <div className="container">
        <div className="row">
          <div className="col-12 col-md-8">
            <Detail news={newsDetail} />
          </div>
          <div className="col-12 col-md-4">
            {/* <h4 className={styles.otherNews}>Berita Lainnya</h4> */}
            {/* <NewsList onePerRow items={otherNews} /> */}
          </div>
        </div>
      </div>
    </section>
  )
}

const Detail = ({ news: { imagePath, createdAt, title, content } }) => (
  <div className={styles.news}>
    <div className={styles.imageWrapper}>
      <img src={imagePath} alt="" />
    </div>
    <div className={styles.contentWrapper}>
      <div className={styles.createdAt}>{moment(createdAt).format('DD MMMM YYYY')}</div>
      <hr />
      <h4 className={styles.title}>{title}</h4>
      <p className={styles.desc} dangerouslySetInnerHTML={{__html: content}}/>
    </div>
  </div>
)

export default NewsDetailSection
