import styles from './layout.module.scss'
import Header from './Header'
import Footer from './Footer'

const Layout = ({ children }) => {
  return (
    <div className={styles.layout}>
      <Header />
      <main>
        {children}
      </main>
      <Footer />
    </div>
  )
}

export default Layout
