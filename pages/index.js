import Layout from '../components/Layout'
import Hero from '../components/Layout/LandingPage/Hero'
import RegisterForm from '../components/Layout/LandingPage/RegisterForm'
import Terms from '../components/Layout/LandingPage/Terms'
import Winner from '../components/Layout/LandingPage/Winner'
import Videos from '../components/Layout/LandingPage/Videos'
import News from '../components/Layout/LandingPage/News'
import Participants from '../components/Layout/LandingPage/Participants'
import Assesment from '../components/Layout/LandingPage/Assesment'
import Category from '../components/Layout/LandingPage/Category'
import Prize from '../components/Layout/LandingPage/Prize'
import Posting from '../components/Layout/LandingPage/Posting'
import { newsData, participantsData, videosData, winnersData } from '../utils/services/dummyData'

export default function Home({ winners, videos, news, participants }) {
  return (
    <Layout>
      <Hero />
      {/* <RegisterForm /> */}
      <Terms />
      <Posting />
      <Prize />
      <Category />
      <Assesment />
      {/* {
        participants.length >= 50 && (
          <Participants participants={participants} />
        )
      } */}
      {/* <News news={news} /> */}
      <Videos videos={videos} />
      {/* <Winner winners={winners} /> */}
    </Layout>
  )
}

export function getServerSideProps(context) {
  // const winners = await winnersApi.list();
  // const videos = await videosApi.list();
  // const { data: news } = await newsApi.list('&per_page=3');
  // const participants = await participantsApi.list() ?? [];

  const winners = winnersData;
  const videos = videosData;
  const news = newsData;
  const participants = participantsData;

  return {
    props: {
      winners,
      videos,
      news,
      participants
    }
  }
}
