import Head from 'next/head'
import '../styles/app.scss'
import Router from 'next/router'
import NProgress from 'nprogress'

NProgress.configure({
  template: `
    <div class="bar-wrapper">
      <div class="bar" role="bar">
        <div class="peg"></div>
      </div>
    </div>
  `
})

Router.events.on('routeChangeStart', () => NProgress.start())
Router.events.on('routeChangeComplete', () => NProgress.done())
Router.events.on('routeChangeError', () => NProgress.done())

const MyApp = ({ Component, pageProps }) => {
  return (
    <>
      <Head>
        <link rel="icon" href="/favicon.ico" />
        <meta name="viewport" content="width=device-width, initial-scale=1" key="viewport" />

        <title>Iniindonesiaku X Pigijo</title>
        <meta name="description" content="Pigijo Microsite" key="description" />

        <meta name="og:title" content="Pigijo Microsite" key="ogTitle" />
        <meta name="og:description" content="Pigijo Microsite" key="ogDescription" />
        <meta name="og:type" content="website" key="ogType" />
        <meta name="og:url" content="https://github.com/hendra-foo/hendra-next-boilerplate" key="ogUrl" />
        <meta name="og:site_name" content="Pigijo Microsite" key="ogSiteName" />
      </Head>
      <Component {...pageProps} />
    </>
  )
}

export default MyApp;