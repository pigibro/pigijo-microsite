import Layout from "../components/Layout"
import { participantsApi } from "../utils/services/api";
import ParticipantListSection from "../components/Layout/ParticipantPage";

const Participant = ({ participants }) => {
  return (
    <Layout>
      <ParticipantListSection participants={participants} />
    </Layout>
  )
}

export async function getServerSideProps(context) {
  const participants = await participantsApi.list();

  return {
    props: {
      participants
    }
  }
}

export default Participant
