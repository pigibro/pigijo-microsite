import { newsApi } from "../../utils/services/api";
import Layout from "../../components/Layout";
import NewsDetailSection from "../../components/Layout/NewsPage/NewsDetail";

const NewsDetail = ({ newsDetail, otherNews }) => {
  return (
    <Layout>
      <NewsDetailSection
        newsDetail={newsDetail}
        otherNews={otherNews}
      />
    </Layout>
  )
}

export async function getServerSideProps({ params: { slug } }) {
  const id = slug.split('-')[0]
  const newsDetail = await newsApi.detail(id);
  const otherNews = [];
  
  return {
    props: {
      newsDetail,
      otherNews
    }
  }
}

export default NewsDetail
