import Layout from "../../components/Layout"
import { newsApi } from "../../utils/services/api";
import NewsListSection from "../../components/Layout/NewsPage";

const NewsList = ({ news }) => {
  return (
    <Layout>
      <NewsListSection news={news} />
    </Layout>
  )
}

export async function getServerSideProps(context) {
  const news = await newsApi.list();

  return {
    props: {
      news
    }
  }
}

export default NewsList
