class VideoType {
  static TUTORIAL = 0;
  static USER_VIDEO = 1;

  static getStr(e) {
    switch (e) {
      case VideoType.TUTORIAL:
        return 'Kuliner';
      case VideoType.USER_VIDEO:
        return 'Sejarah';
      default:
        return 'Unknown';
    }
  }
}

export { VideoType };