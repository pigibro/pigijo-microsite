import { axiosInstance } from "./axios";
import { jsonToFormData } from "../helpers/jsonToFormData";
import { winnersData, videosData, news, participants } from "./dummyData";

export const authApi = {
  register: ( data ) => {
    // Data transformed to Form Data due to file posting
    const d = jsonToFormData(data);
    
    // POST Request Example    
    return axiosInstance.post('/api/register', d);
  }
}

export const winnersApi = {
  list: () => {
    // Fake Success API Callback
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(winnersData)
      }, 150);
    })

    // GET Request Example
    // return axiosInstance.get('/{endpoint}')
  },
}

export const videosApi = {
  list: () => {
    // Fake Success API Callback
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(videosData)
      }, 150);
    })

    // GET Request Example
    // return axiosInstance.get('/{endpoint}')
  }
}

export const newsApi = {
  list: (limit) => {

    // GET Request Example
    return axiosInstance.get('/api/allnews?sort_by=title&order=asc' + (limit ? limit : ''))
    // return axiosInstance.get('/{endpoint}')
  },
  detail: (id) => {

    // GET Request Example
    return axiosInstance.get('/api/news/' + id)
  }
}

export const participantsApi = {
  list: (query) => {

    // GET Request Example
    return axiosInstance.get('/api/users?sort_by=name&order=asc')
  }
}
