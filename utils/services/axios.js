import axios from 'axios';

const toCamelCase = (obj) => {
  let rtn = obj
  if (obj !== null && typeof (obj) === 'object') {
    if (obj instanceof Array) {
      rtn = obj.map(toCamelCase)
    } else {
      rtn = {}
      for (let key in obj) {
        if (obj.hasOwnProperty(key)) {
          const newKey = key.charAt(0) !== '_' ? key.replace(/(_\w)/g, k => k[1].toUpperCase()) : key;
          rtn[newKey] = toCamelCase(obj[key])
        }
      }
    }
  }
  return rtn
}

const axiosInstance = axios.create({
  baseURL: process.env.NEXT_PUBLIC_API_END_POINT,
});

axiosInstance.defaults.headers.post['Content-Type'] = 'application/json';
axiosInstance.interceptors.response.use(function (response) {
  // Any status code that lie within the range of 2xx cause this function to trigger
  // Do something with response data
  console.log('response success');
  return toCamelCase(response.data);
}, function (error) {
  // Any status codes that falls outside the range of 2xx cause this function to trigger
  // Do something with response error
  console.log('response error');
  const err = error.response?.data ?? {
    status: "error",
    code: 500,
    message: 'Something went wrong, please try again later'
  }
  return Promise.reject(err);
});

const setToken = token => axiosInstance.defaults.headers.common['Authorization'] = token ? `Bearer ${token}` : null

export {
  axiosInstance,
  setToken
}