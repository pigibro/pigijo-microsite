export const winnersData = [
  {
    rank: 1,
    name: "Silvia",
    institution: "Dinas Pendidikan A",
    videoTitle: "Hidup untuk Traveling",
  },
  {
    rank: 2,
    name: "Zulkarnain",
    institution: "Dinas Pendidikan B",
    videoTitle: "Jalan-Jalan, Man!",
  },
  {
    rank: 3,
    name: "Debora Iskandar",
    institution: "Dinas Pendidikan C",
    videoTitle: "Menentukan Mau Pergi Kemana",
  },
  {
    rank: 4,
    name: "Pradipta Cahya",
    institution: "Dinas Pendidikan D",
    videoTitle: "Indonesia Ini Indonesia",
  },
  {
    rank: 5,
    name: "Murniawanti",
    institution: "Dinas Pendidikan B",
    videoTitle: "HUT RI Travel",
  },
  {
    rank: 6,
    name: "Solihin Agustian",
    institution: "Dinas Pendidikan C",
    videoTitle: "1 2 3 Go!",
  },
  {
    rank: 7,
    name: "Steven Pangabean",
    institution: "Dinas Pendidikan D",
    videoTitle: "Bali: Objek Wisata Tersembunyi Di Tanah Lot",
  },
  {
    rank: 8,
    name: "Zulkifri Ahmad",
    institution: "Dinas Pendidikan C",
    videoTitle: "Eksplorasi Laut Jawa",
  },
  {
    rank: 9,
    name: "Johan Setiawan",
    institution: "Dinas Pendidikan D",
    videoTitle: "Selat Sunda",
  },
  {
    rank: 10,
    name: "Andreas Kusumawari",
    institution: "Dinas Pendidikan D",
    videoTitle: "Ekspedisi Travel",
  },
]

export const videosData = [
  {
    videoId: "VQ54VaRqrkk",
    thumbnail: 'https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/iniindonesiaku/Petak+Sembilan.JPG',
    title: "Pasar Petak Sembilan",
    desc: "",
    type: 0,
    date: "2020-07-21",
  },
  {
    videoId: "93Db1OsPI_w",
    thumbnail: "https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/iniindonesiaku/Musem+Sejarah+Kota+Tua+Jakarta.png",
    title: "Museum Sejarah Kota Tua Jakarta",
    desc: "",
    type: 1,
    date: "2020-07-21",
  },
  {
    videoId: "dq55RYCZQl4",
    thumbnail: "https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/iniindonesiaku/Es+Kopi+Tak+Kie.JPG",
    title: "Es Kopi Tak Kie",
    desc: "",
    type: 0,
    date: "2020-07-21",
  },
  {
    videoId: "2_aVdJFCwYg",
    thumbnail: "https://pigijo.s3-ap-southeast-1.amazonaws.com/assets/images/iniindonesiaku/Sore+di+Raja+Ampat.JPG",
    title: "Sore Di Raja Ampat",
    desc: "",
    type: 0,
    date: "2020-07-23",
  },
]

export const newsData = [
  {
    imageSrc: "/images/news/1.png",
    title: "Kementerian Pariwisata dan Ekonomi Kreatif",
    desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In luctus sodales finibus. Donec eu nunc dictum, iaculis dui eget, mattis orci. Nunc lacinia pulvinar ipsum eget consectetur. Phasellus augue ex, porttitor nec velit in, consequat blandit mi. Cras dolor augue, fringilla et velit at, venenatis pulvinar quam. Suspendisse at hendrerit mauris. Sed vestibulum pulvinar leo. Proin malesuada faucibus ligula, ac blandit ante feugiat eu. Pellentesque vitae justo finibus, tincidunt mauris in, ultrices lectus. Etiam eu malesuada nibh, sit amet vestibulum lectus. Sed tempor lacus vitae pharetra lobortis. Morbi consectetur eu dui sit amet faucibus. Aliquam erat volutpat. Mauris justo lacus, pharetra ut sagittis eget, ultrices non elit. Duis volutpat augue lorem, vitae auctor risus euismod in. Suspendisse in nulla lorem. Donec varius magna turpis. Nunc volutpat ex gravida ligula mollis, vel elementum elit congue. Donec nulla odio, sollicitudin sed diam lacinia, consequat gravida risus. Cras luctus eleifend finibus. In vel sem lorem. Donec ut faucibus urna. Vestibulum posuere dictum enim sed mollis. Nunc fermentum consequat mauris sed sodales. In pretium tortor nisl, eu cursus dui pharetra eget. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Proin lobortis luctus fringilla. Donec scelerisque elit ipsum, id convallis tortor euismod vitae. Fusce ac dui porttitor, dignissim diam eu, mattis dui. Duis commodo sed nibh ac sollicitudin. Interdum et malesuada fames ac ante ipsum primis in faucibus. In egestas lacus ac metus placerat volutpat. Curabitur eu rutrum augue. Maecenas mattis facilisis tortor, eget fermentum ligula vulputate in. Donec egestas dui id ante bibendum, vel varius sapien aliquam. Quisque tincidunt laoreet justo tempor dictum. Integer ac ornare ligula, sit amet fringilla nisi. Nunc vitae dolor scelerisque, bibendum nibh ut, posuere nunc. Phasellus rhoncus ligula a arcu pulvinar malesuada. Etiam ut maximus risus. Proin vitae augue non tellus facilisis imperdiet. In hac habitasse platea dictumst. Aenean pharetra risus ac purus aliquet, ut convallis tortor congue. Sed ultricies suscipit sapien et commodo. In mattis quam venenatis porta pulvinar. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis finibus bibendum urna, vel sollicitudin enim iaculis sed. Aliquam convallis, velit eu rutrum sollicitudin, est metus consequat erat, id pellentesque eros tellus vel velit.",
    date: "2020-06-11",
    slug: 'kementrian-pariwisata-dan-ekonomi-kreatif'
  },
  {
    imageSrc: "/images/news/2.png",
    title: "Ajang Kompetisi Pariwisata se-ASEAN Digelar di Indonesia",
    desc: "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
    date: "2020-05-23",
    slug: 'ajang-kompetisi-pariwisata-se-asean-digelar-di-indonesia'
  },
  {
    imageSrc: "/images/news/3.png",
    title: "Kompetisi Videogram ala Eka Putra",
    desc: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
    date: "2020-04-06",
    slug: 'kompetisi-videogram-ala-eka-putra'
  },
]

export const participantsData = [
  {
    name: "Septian Nurhaidin",
    institution: "Dinas Pendidikan A",
  },
  {
    name: "Budiman Wijaya",
    institution: "Dinas Pendidikan B",
  },
  {
    name: "Meidyna Roselinni",
    institution: "Dinas Pendidikan C",
  },
  {
    name: "Artika Windy",
    institution: "Dinas Pendidikan D",
  },
]
