const currency = Intl.NumberFormat('in-ID', { style: 'currency', currency: 'IDR', minimumFractionDigits: 0 });
const number = Intl.NumberFormat('in-ID', { minimumFractionDigits: 0 });

export const toIDR = (v, symbol = true) => {
  let modV = v;
  
  if (!isNaN(v)) {
    modV = symbol ? currency.format(v) : number.format(v);
  }

  return modV
}